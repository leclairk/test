
/**
Filename: timer.h
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

/**
This file contains all of the addresses for the registers, as well as the setupRegs function prototype.
*/

#ifndef TIMER_H
#define TIMER_H

#include "main.h"

void timer_init(void);
int16_t timer_start(void);
int16_t timer_stop(int16_t start_time);
void timer_shutdown(void);


#endif

