Kiley
Tessa

This program simualtes the Cylon lights from Battle Star Galactica for lab 1

registers.h - contains the #define statements
registers.c - casts the defined values in registers.h to volatile uint32_t
main.h - definitions for external volatile uint32_t and the function header for delay()
main.c - contains the main function delay function. The delay function causes the program to do nothing for a short period of time
        The main function contains a while loop that turns a light on and turns a light off and does the same for each sequential light
        
to compile the program run 
use uVision
