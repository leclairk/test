Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab2


Filename: main.c
- The main function initializes the values of
  many variables, then determines what input the user selected.
  Finally, it resets everything.
- The print_string function takes a const char array,
  then prints the array. It returns nothing.
- The TO_UPPER function takes a char data
  and converts it to uppercase.
- The Good_To_Go function takes an integer, i,
  which is the loop counter from the main function,
  prints a new line, and sets i to 100.
- The get_status function takes an integer,
  then determines which light the user
  would like to view the status of using a case statement.
  It returns nothing.
- The on function takes an integer,
  then determines which light the user
  would like to turn on using a case statement.
  It returns nothing.
- The off function takes an integer,
  then determines which light the user
  would like to turn off using a case statement.
  It returns nothing.
- The print_time function prints the time by calling the sendbyte function.
  It returns nothing.
- The print_date function prints the date by calling the sendbyte function.
  It returns nothing
- The Restart function takes a character array and resets
  all of its values to a null character.
  It also resets the GPIOA_CRL register.
  It returns nothing.

Filename: serial_driver_interface_c.c
Information in this section is obrained from the lab handout made by Karim Naqvi.
- The serial_open function configures and enables the device.
- The send_byte function sends an 8-bit byte to the serial port,
  using the configured bit-rate, # of bits, etc
  Returns 0 on success and non-zero on failure.
- The get_byte function obrains an 8-bit character from the serial port, and returns it.

COMPILING
To compile this program, use Keil Tools.
