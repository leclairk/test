#ifndef ARITHMETIC_H
#define ARITHMETIC_H

#include "main.h"

uint32_t add_32(uint32_t, uint32_t);


//I added these


uint64_t add_64(uint32_t, uint32_t);

uint32_t multiply_32(uint32_t, uint32_t);
uint64_t multiply_64(uint32_t, uint32_t);

uint32_t divide_32(uint32_t, uint32_t);
uint64_t divide_64(uint32_t, uint32_t);

void copy_8_byte_struct(void);
void copy_128_byte_struct(void);
void copy_1024_byte_struct(void);


uint32_t gen_random_32(void);



struct a_8_byte_struct {
    uint8_t byte_1;
    uint8_t byte_2;
    uint8_t byte_3;
    uint8_t byte_4;
    uint8_t byte_5;
    uint8_t byte_6;
    uint8_t byte_7;
    uint8_t byte_8;
}; // sizeof(a_8_byte_struct) = 8

struct a_128_byte_struct {
    uint64_t byte_1;
    uint64_t byte_2;
    uint64_t byte_3;
    uint64_t byte_4;
    uint64_t byte_5;
    uint64_t byte_6;
    uint64_t byte_7;
    uint64_t byte_8;
	  uint64_t byte_9;
		uint64_t byte_10;
    uint64_t byte_12;
    uint64_t byte_13;
    uint64_t byte_14;
    uint64_t byte_15;
    uint64_t byte_16;
 
}; // sizeof(a128_byte_struct) =128

struct a_1024_byte_struct {
    uint8_t byte_1;
    uint8_t byte_2;
    uint8_t byte_3;
    uint8_t byte_4;
    uint8_t byte_5;
    uint8_t byte_6;
    uint8_t byte_7;
    uint8_t byte_8;
}; // sizeof(a_8_byte_struct) = 8


//I added these ^^



#endif
